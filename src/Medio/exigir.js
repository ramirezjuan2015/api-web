const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const User = mongoose.model("User");

module.exports = (req, res, next) => {
  const { authorization } = req.headers;

  if (!authorization) {
    return res.status(401).send({ error: "Usted debe estar conectado." });
  }

  const token = authorization.replace("Bearer ", "");
  jwt.verify(token, "MY_SECRET_KEY", async (err, payload) => {
    if (err) {
      return res.status(401).send({ error: "Usted debe estar conectado." });
    }

    const { userId } = payload;
    console.log("userId", userId);
    const user = await User.findById(userId);
    if (!user) return res.status(401).send({ error: "Usuario no existe" });
    req.user = user;
    next();
  });
};
